package ru.t1.avfilippov.tm.command.system;

import ru.t1.avfilippov.tm.api.model.ICommand;
import ru.t1.avfilippov.tm.command.AbstractCommand;
import ru.t1.avfilippov.tm.model.Command;

import java.util.Collection;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @Override
    public String getArgument() {
        return "-h";
    }

    @Override
    public String getDescription() {
        return "display list of terminal commands";
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) System.out.println(command);
    }

}
