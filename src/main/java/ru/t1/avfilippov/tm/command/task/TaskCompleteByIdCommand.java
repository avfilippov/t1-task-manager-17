package ru.t1.avfilippov.tm.command.task;

import ru.t1.avfilippov.tm.enumerated.Status;
import ru.t1.avfilippov.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand{

    @Override
    public String getDescription() {
        return "complete task by id";
    }

    @Override
    public String getName() {
        return "task-complete-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        getTaskService().changeTaskStatusById(id, Status.COMPLETED);
    }
}
